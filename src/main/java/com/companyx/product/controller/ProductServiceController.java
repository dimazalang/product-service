package com.companyx.product.controller;


import com.companyx.product.model.Product;
import com.fasterxml.jackson.databind.util.JSONPObject;
import lombok.AllArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RequestMapping("/api/v1/public/product")
@RestController
@AllArgsConstructor
public class ProductServiceController {

    private RestTemplate restTemplate;

    @GetMapping(path = "/greet")
    public ResponseEntity<String> greet() {

        return new ResponseEntity<String>("Welcome to Product Service", HttpStatus.OK);
    }

    @GetMapping(path = "/reviews", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Product>> listOfProductReviews() {
        final String urlFetchAll = "http://localhost:8181/api/v1/public/product/reviews";

        ResponseEntity<List<Product>> allProductReviews = restTemplate.exchange(
                urlFetchAll, HttpMethod.GET, null, new ParameterizedTypeReference<List<Product>>() {
        });

        return allProductReviews;
    }

    @GetMapping(path = "/review/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> fetchProductReview(@PathVariable("id") Long id) {
        final String urlFetchAll = "http://localhost:8181/api/v1/public/product/review/{id}";

        Product prod = restTemplate.getForObject(urlFetchAll, Product.class, id);
        if ( prod != null ) {
            return new ResponseEntity<Product>(prod, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

}
